from apscheduler.schedulers.blocking import BlockingScheduler
from pytz import utc

from src import core


def twitter_report():
    user = core.twitter_api.get_user_by_username("axaryk")
    followers_update = core.twitter_api.get_followers_by_id(user.id)
    following_update = core.twitter_api.get_following(user.id)
    core.repository.save_twitter_user(user)
    core.repository.save_batch_twitter_users(followers_update)
    core.repository.save_batch_twitter_users(following_update)

    followers_old = core.repository.get_batch_followers_ids_of(user)
    # following_old = core.repository.get_batch_following_ids_of(user)

    # unfollowers = followers_old.difference(followers_update)

    # print(unfollowers)

    core.repository.save_batch_followers_of(user, followers_update)
    core.repository.save_batch_following_of(user, following_update)


def cron():
    scheduler = BlockingScheduler()
    scheduler.add_job(dummy, 'cron', day='*', hour=20, minute=30, timezone=utc)
    scheduler.start()


def dummy():
    print("Hello")


if __name__ == '__main__':
    # Create an instance of scheduler and add function.
    twitter_report()
