from os import environ

from dotenv import dotenv_values

CONFIG = {**dotenv_values(".env"), **environ}
