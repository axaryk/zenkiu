from config import CONFIG
from src.repository.repository import Repository

repository = Repository(host=CONFIG.get("DATABASE_HOST"),
                        database=CONFIG.get("DATABASE_NAME"),
                        port=CONFIG.get("DATABASE_PORT"),
                        user=CONFIG.get("DATABASE_USER"),
                        password=CONFIG.get("DATABASE_PASSWORD"))
