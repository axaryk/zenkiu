from typing import Set

from psycopg2 import connect
from psycopg2.extras import execute_values

from src.core.twitter_user import TwitterUser


class Repository:
    def __init__(
            self,
            host: str,
            database: str,
            user: str,
            password: str,
            port: int = 5432):
        self.conn = connect(
            host=host,
            dbname=database,
            user=user,
            password=password,
            port=port)
        self.conn.autocommit = True

    def save_twitter_user(self, user: TwitterUser):
        sql_sentence = """INSERT INTO twitter_user
                (id,
                name,
                username,
                created_at,
                description,
                location,
                profile_image_url,
                url,
                protected,
                followers_count, 
                following_count, 
                tweet_count, 
                listed_count, 
                verified) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (id) DO
                UPDATE SET 
                name = EXCLUDED.name,
                description = EXCLUDED.description,
                location = EXCLUDED.location,
                profile_image_url = EXCLUDED.profile_image_url,
                url = EXCLUDED.url,
                protected = EXCLUDED.protected,
                followers_count = EXCLUDED.followers_count, 
                following_count = EXCLUDED.following_count, 
                tweet_count = EXCLUDED.tweet_count, 
                listed_count = EXCLUDED.listed_count, 
                verified = EXCLUDED.verified,
                updated_at = CURRENT_TIMESTAMP"""

        values = (user.id,
                  user.name,
                  user.username,
                  user.created_at,
                  user.description,
                  user.location,
                  user.profile_image_url,
                  user.url,
                  user.protected,
                  user.followers_count,
                  user.following_count,
                  user.tweet_count,
                  user.listed_count,
                  user.verified)

        with self.conn.cursor() as cur:
            cur.execute(sql_sentence, values)

    def save_batch_twitter_users(self, twitter_users: Set[TwitterUser]):
        sql_sentence = """INSERT INTO twitter_user
        (id,
        name,
        username,
        created_at,
        description,
        location,
        profile_image_url,
        url,
        protected,
        followers_count, 
        following_count, 
        tweet_count, 
        listed_count, 
        verified) VALUES %s ON CONFLICT (id) DO
        UPDATE SET 
        name = EXCLUDED.name,
        description = EXCLUDED.description,
        location = EXCLUDED.location,
        profile_image_url = EXCLUDED.profile_image_url,
        url = EXCLUDED.url,
        protected = EXCLUDED.protected,
        followers_count = EXCLUDED.followers_count, 
        following_count = EXCLUDED.following_count, 
        tweet_count = EXCLUDED.tweet_count, 
        listed_count = EXCLUDED.listed_count, 
        verified = EXCLUDED.verified,
        updated_at = CURRENT_TIMESTAMP"""

        batch = [(user.id,
                  user.name,
                  user.username,
                  user.created_at,
                  user.description,
                  user.location,
                  user.profile_image_url,
                  user.url,
                  user.protected,
                  user.followers_count,
                  user.following_count,
                  user.tweet_count,
                  user.listed_count,
                  user.verified) for user in twitter_users]

        with self.conn.cursor() as cur:
            execute_values(cur, sql_sentence, batch)

    def save_batch_followers_of(self, user: TwitterUser, followers: Set[TwitterUser]):
        sql_sentence = """INSERT INTO twitter_follows 
        (id_twitter_user, 
        id_twitter_user_follow) VALUES %s ON CONFLICT (id_twitter_user, id_twitter_user_follow) DO 
        UPDATE SET 
        updated_at = CURRENT_TIMESTAMP
        """

        batch = [(follower.id,
                  user.id) for follower in followers]

        with self.conn.cursor() as cur:
            execute_values(cur, sql_sentence, batch)

    def save_batch_following_of(self, user: TwitterUser, following: Set[TwitterUser]):
        sql_sentence = """INSERT INTO twitter_follows 
        (id_twitter_user, 
        id_twitter_user_follow) VALUES %s ON CONFLICT (id_twitter_user, id_twitter_user_follow) DO 
        UPDATE SET 
        updated_at = CURRENT_TIMESTAMP
        """

        batch = [(user.id,
                  follow.id) for follow in following]

        with self.conn.cursor() as cur:
            execute_values(cur, sql_sentence, batch)

    def get_batch_followers_ids_of(self, user: TwitterUser) -> Set[str]:
        sql_sentence = """SELECT id_twitter_user
        FROM twitter_follows 
        WHERE id_twitter_user_follow = %s"""

        with self.conn.cursor() as cur:
            cur.execute(sql_sentence, (user.id,))
            return {user_id for user_id in cur.fetchall()}

    def get_batch_following_ids_of(self, user: TwitterUser) -> Set[str]:
        sql_sentence = """SELECT id_twitter_user_follow
                FROM twitter_follows 
                WHERE id_twitter_user = %s"""

        with self.conn.cursor() as cur:
            cur.execute(sql_sentence, (user.id,))
            return {user_id for user_id in cur.fetchall()}
