from abc import ABCMeta


class RepositoryInterface(metaclass=ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'save_twitter_user') and
                callable(subclass.save_twitter_user) and
                hasattr(subclass, 'save_batch_twitter_users') and
                callable(subclass.save_batch_twitter_users) and
                hasattr(subclass, 'save_batch_followers_of') and
                callable(subclass.save_batch_followers_of) and
                hasattr(subclass, 'save_batch_following_of') and
                callable(subclass.save_batch_following_of) and
                hasattr(subclass, 'get_batch_followers_ids_of') and
                callable(subclass.get_batch_followers_ids_of) and
                hasattr(subclass, 'get_batch_following_ids_of') and
                callable(subclass.get_batch_following_ids_of)

                or
                NotImplemented)
