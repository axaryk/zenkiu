from config import CONFIG
from src.bot.bot import Bot

bot = Bot(token=CONFIG.get("TELEGRAM_BOT_TOKEN"))
