from dataclasses import dataclass

from telegram.ext import Updater, CommandHandler, Dispatcher

from src.bot.commands import start, help_command


@dataclass
class Bot:
    updater: Updater
    dispatcher: Dispatcher

    def __init__(self, token: str) -> None:
        self.updater = Updater(token=token)
        self.dispatcher = self.updater.dispatcher

    def __register_all_handlers(self):
        self.dispatcher.add_handler(CommandHandler("start", start))
        self.dispatcher.add_handler(CommandHandler("help", help_command))

    def start_polling(self) -> None:
        self.__register_all_handlers()
        self.updater.start_polling()
        self.updater.idle()
