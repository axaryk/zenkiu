from typing import Set

from src.twitter.api_interface import TwitterAPIInterface


class TwitterReciprocation:
    def __init__(self, twitter_api: TwitterAPIInterface, repository: RepositoryInterface):
        pass

    def detect_unfollowers(self, following_ids: Set[str], followers_ids: Set[str]) -> Set[str]:
        return following_ids.difference(followers_ids)

    def detect_fans(self, following_ids: Set[str], followers_ids: Set[str]) -> Set[str]:
        return followers_ids.difference(following_ids)

    def mutuals(self, following_ids: Set[str], followers_ids: Set[str]) -> Set[str]:
        return following_ids.intersection(followers_ids)
