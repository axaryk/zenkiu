from dataclasses import dataclass

from src.repository import Repository
from src.twitter import TwitterAPI


@dataclass
class Core:
    repository: Repository
    twitter_api: TwitterAPI
