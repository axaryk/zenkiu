from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class TwitterUser:
    id: str
    name: str
    username: str
    created_at: datetime
    description: Optional[str] = None
    location: Optional[str] = None
    profile_image_url: Optional[str] = None
    url: Optional[str] = None
    protected: Optional[bool] = False
    followers_count: Optional[int] = 0
    following_count: Optional[int] = 0
    tweet_count: Optional[int] = 0
    listed_count: Optional[int] = 0
    verified: Optional[bool] = False
    registered_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None

    def __eq__(self, other):
        if not isinstance(other, TwitterUser):
            return NotImplemented
        return self.id.__eq__(other.id)
