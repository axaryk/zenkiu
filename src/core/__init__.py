from src.core.core import Core
from src.repository import repository
from src.twitter import twitter_api

core = Core(repository=repository, twitter_api=twitter_api)
