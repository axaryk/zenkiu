from config import CONFIG
from src.twitter.api import TwitterAPI

twitter_api = TwitterAPI(consumer_key=CONFIG.get("TWITTER_CONSUMER_KEY"),
                         consumer_secret=CONFIG.get("TWITTER_CONSUMER_SECRET"),
                         access_token=CONFIG.get("TWITTER_ACCESS_TOKEN"),
                         token_secret=CONFIG.get("TWITTER_TOKEN_SECRET"))
