from typing import List, Optional

from pytwitter import Api

from src.core.twitter_user import TwitterUser
from src.twitter.api_interface import TwitterAPIInterface


class TwitterAPI(TwitterAPIInterface):
    def __init__(self,
                 consumer_key: str,
                 consumer_secret: str,
                 access_token: str,
                 token_secret: str):
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token = access_token
        self.token_secret = token_secret
        self.api = Api(consumer_key=self.consumer_key,
                       consumer_secret=self.consumer_secret,
                       access_token=self.access_token,
                       access_secret=self.token_secret,
                       oauth_flow=True,
                       sleep_on_rate_limit=True)
        self.user_fields = ("id",
                            "name",
                            "username",
                            "created_at",
                            "description",
                            "location",
                            "profile_image_url",
                            "protected",
                            "public_metrics",
                            "url",
                            "verified")

    def get_user_by_username(self, twitter_username: str) -> Optional[TwitterUser]:
        user = self.api.get_user(username=twitter_username, user_fields=self.user_fields).data
        return TwitterUser(id=user.id,
                           name=user.name,
                           username=user.username,
                           created_at=user.created_at,
                           description=user.description,
                           location=user.location,
                           profile_image_url=user.profile_image_url,
                           url=user.url,
                           protected=user.protected,
                           followers_count=user.public_metrics.followers_count,
                           following_count=user.public_metrics.following_count,
                           tweet_count=user.public_metrics.tweet_count,
                           listed_count=user.public_metrics.listed_count,
                           verified=user.verified)

    def get_user_by_id(self, twitter_user_id: str) -> Optional[TwitterUser]:
        user = self.api.get_user(user_id=twitter_user_id, user_fields=self.user_fields).data
        return TwitterUser(id=user.id,
                           name=user.name,
                           username=user.username,
                           created_at=user.created_at,
                           description=user.description,
                           location=user.location,
                           profile_image_url=user.profile_image_url,
                           url=user.url,
                           protected=user.protected,
                           followers_count=user.public_metrics.followers_count,
                           following_count=user.public_metrics.following_count,
                           tweet_count=user.public_metrics.tweet_count,
                           listed_count=user.public_metrics.listed_count,
                           verified=user.verified)

    def get_followers(self, twitter_user_id: str) -> List[TwitterUser]:
        return [TwitterUser(id=user.id,
                            name=user.name,
                            username=user.username,
                            created_at=user.created_at,
                            description=user.description,
                            location=user.location,
                            profile_image_url=user.profile_image_url,
                            url=user.url,
                            protected=user.protected,
                            followers_count=user.public_metrics.followers_count,
                            following_count=user.public_metrics.following_count,
                            tweet_count=user.public_metrics.tweet_count,
                            listed_count=user.public_metrics.listed_count,
                            verified=user.verified) for user in self.api.get_followers(user_id=twitter_user_id,
                                                                                       user_fields=self.user_fields,
                                                                                       max_results=1000).data]

    def get_following(self, twitter_user_id: str) -> List[TwitterUser]:
        return [TwitterUser(id=user.id,
                            name=user.name,
                            username=user.username,
                            created_at=user.created_at,
                            description=user.description,
                            location=user.location,
                            profile_image_url=user.profile_image_url,
                            url=user.url,
                            protected=user.protected,
                            followers_count=user.public_metrics.followers_count,
                            following_count=user.public_metrics.following_count,
                            tweet_count=user.public_metrics.tweet_count,
                            listed_count=user.public_metrics.listed_count,
                            verified=user.verified) for user in self.api.get_following(user_id=twitter_user_id,
                                                                                       user_fields=self.user_fields,
                                                                                       max_results=1000).data]
