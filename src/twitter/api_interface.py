from abc import ABCMeta, abstractmethod
from typing import List, Optional

from src.core.twitter_user import TwitterUser


class TwitterAPIInterface(metaclass=ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'get_user_by_username') and
                callable(subclass.get_user_by_username) and
                hasattr(subclass, 'get_user_by_id') and
                callable(subclass.get_user_by_id) and
                hasattr(subclass, 'get_followers') and
                callable(subclass.get_followers) and
                hasattr(subclass, 'get_following') and
                callable(subclass.get_following) or
                NotImplemented)

    @abstractmethod
    def get_user_by_username(self, twitter_username: str) -> Optional[TwitterUser]:
        raise NotImplementedError

    @abstractmethod
    def get_user_by_id(self, twitter_user_id: str) -> Optional[TwitterUser]:
        raise NotImplementedError

    @abstractmethod
    def get_followers(self, twitter_user_id: str) -> List[TwitterUser]:
        raise NotImplementedError

    @abstractmethod
    def get_following(self, twitter_user_id: str) -> List[TwitterUser]:
        raise NotImplementedError
