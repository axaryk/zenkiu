from typing import Set, Optional

from src.core.twitter_user import TwitterUser
from src.twitter.api_interface import TwitterAPIInterface


class TwitterAPIFake(TwitterAPIInterface):
    def __init__(self, followers: Set[TwitterUser], following: Set[TwitterUser]):
        self.followers = followers
        self.following = following
        pass

    def get_user_by_id(self, twitter_user_id: str) -> Optional[TwitterUser]:
        pass

    def get_followers(self, twitter_user_id: str) -> Set[TwitterUser]:
        return self.followers

    def get_following(self, twitter_user_id: str) -> Set[TwitterUser]:
        return self.following

    def get_user_by_username(self, twitter_username: str) -> Optional[TwitterUser]:
        pass
